from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html', images=[1, 2, 3, 4, 5])


@app.route('/', methods=['POST'])
def save():
    print dict(request.form)
    # some logic starts here....
    return render_template('index.html', images=[1, 2, 3, 4, 5])


if __name__ == '__main__':
    app.run(debug=True)
